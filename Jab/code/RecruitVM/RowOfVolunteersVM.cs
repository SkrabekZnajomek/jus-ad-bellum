﻿using System;
using System.Collections.Generic;
using TaleWorlds.Library;
using TaleWorlds.Core;
using TaleWorlds.CampaignSystem;
using JusAdBellum.Utility;
using JusAdBellum.CultureUnitLists;
using TaleWorlds.Core.ViewModelCollection.Information;
using TaleWorlds.CampaignSystem.ViewModelCollection.GameMenu.Recruitment;

namespace JusAdBellum
{
    class RowOfVolunteersVM : ViewModel
    {
        private int numberOfAvailablePesants;

        private int numberOfAvailableNobles;

        private int maxAllowedPesants;
        private int maxAllowedNobles;

        private readonly Action<RowOfVolunteersVM, VolunteerVM> _onRecruit;

        private readonly Action<RowOfVolunteersVM, VolunteerVM> _onRemoveFromCart;

        private string _quantityText;

        private string _recruitText;

        private bool _canRecruit;

        private bool _buttonIsVisible;

        private HintViewModel _recruitHint;

        private RecruitVolunteerOwnerVM _owner;

        private MBBindingList<VolunteerVM> _troops;

        private string _numberOfVolunteersText;

        [DataSourceProperty]
        public string NumberOfVolunteersText
        {
            get
            {
                return _numberOfVolunteersText;
            }
            set
            {
                if (value != _numberOfVolunteersText)
                {
                    _numberOfVolunteersText = value;
                    OnPropertyChangedWithValue(value, "NumberOfVolunteersText");
                }
            }
        }
        public Hero OwnerHero
        {
            get;
            private set;
        }

        public List<CharacterObject> VolunteerTroops
        {
            get;
            private set;
        }

        public int GoldCost
        {
            get;
        }

        [DataSourceProperty]
        public MBBindingList<VolunteerVM> Troops
        {
            get
            {
                return _troops;
            }
            set
            {
                if (value != _troops)
                {
                    _troops = value;
                    OnPropertyChangedWithValue(value, "Troops");
                }
            }
        }

        [DataSourceProperty]
        public RecruitVolunteerOwnerVM Owner
        {
            get
            {
                return _owner;
            }
            set
            {
                if (value != _owner)
                {
                    _owner = value;
                    OnPropertyChangedWithValue(value, "Owner");
                }
            }
        }

        [DataSourceProperty]
        public bool CanRecruit
        {
            get
            {
                return _canRecruit;
            }
            set
            {
                if (value != _canRecruit)
                {
                    _canRecruit = value;
                    OnPropertyChangedWithValue(value, "CanRecruit");
                }
            }
        }

        [DataSourceProperty]
        public bool ButtonIsVisible
        {
            get
            {
                return _buttonIsVisible;
            }
            set
            {
                if (value != _buttonIsVisible)
                {
                    _buttonIsVisible = value;
                    OnPropertyChangedWithValue(value, "ButtonIsVisible");
                }
            }
        }

        [DataSourceProperty]
        public string QuantityText
        {
            get
            {
                return _quantityText;
            }
            set
            {
                if (value != _quantityText)
                {
                    _quantityText = value;
                    OnPropertyChangedWithValue(value, "QuantityText");
                }
            }
        }

        [DataSourceProperty]
        public string RecruitText
        {
            get
            {
                return _recruitText;
            }
            set
            {
                if (value != _recruitText)
                {
                    _recruitText = value;
                    OnPropertyChangedWithValue(value, "RecruitText");
                }
            }
        }

        [DataSourceProperty]
        public HintViewModel RecruitHint
        {
            get
            {
                return _recruitHint;
            }
            set
            {
                if (value != _recruitHint)
                {
                    _recruitHint = value;
                    OnPropertyChangedWithValue(value, "RecruitHint");
                }
            }
        }

        public bool HavePeasantsAvailable ()
        {
            if (numberOfAvailablePesants > 0)
                return true;
            return false;
        }

        public bool HaveNoblesAvailable ()
        {
            if (numberOfAvailableNobles > 0)
                return true;
            return false;
        }

        public void RefreshNumberOfRecruits()
        {
            NumberOfVolunteersText = $"{numberOfAvailablePesants} - {numberOfAvailableNobles}";
        }

        public void recruitAll()
        {
            for(int i = Troops.Count - 1; i >= 0; i--)
            {
                if (Troops[i].UnitTier == UnitType.NobleUnit && Troops[i].CanBeRecruited)
                {
                    int numberOfRecruitsToGet = numberOfAvailableNobles;
                    for (int j = 0; j < numberOfRecruitsToGet; j++)
                    {
                        Troops[i].ExecuteRecruit();
                    }
                    continue;
                }

                if (Troops[i].CanBeRecruited)
                {
                    JL.Log(JLT.RecruitmentUI, $"searching troop to recruit, i: {i}, {Troops[i].CanBeRecruited}, {numberOfAvailablePesants}");
                    int numberOfRecruitsToGet = numberOfAvailablePesants;
                    for (int j = 0; j < numberOfRecruitsToGet; j++)
                    {
                        Troops[i].ExecuteRecruit();
                    }
                    break;
                }
            }
        }

        public void CheckCanBeRecruited()
        {
            foreach (VolunteerVM troop in _troops)
            {
                if (troop != null)
                {
                    // if ((troop.UnitTier == UnitType.NobleUnit))
                    //     JL.LogUI($"name {_owner.Hero.Name} nobles: {numberOfAvailableNobles} can recruit: {Jab.CD[OwnerHero.Culture].CanPlayerRecruitUnitTier(troop.UnitTier, _owner.Hero)}");
                    if (troop.Character != null && Jab.CD[OwnerHero.Culture].CanPlayerRecruitUnitTier(troop.UnitTier, _owner.Hero) &&
                        ((troop.UnitTier == UnitType.NobleUnit)? HaveNoblesAvailable () : HavePeasantsAvailable ()))
                    {
                        troop.CanBeRecruited = true;
                    }
                    else
                    {
                        troop.CanBeRecruited = false;
                    }
                }
            }
        }

        public RowOfVolunteersVM(Hero owner, List<UnitType> troops, Action<RowOfVolunteersVM, VolunteerVM> onRecruit, Action<RowOfVolunteersVM, VolunteerVM> onRemoveFromCart)
        {
            OwnerHero = owner;
            _onRecruit = onRecruit;
            _onRemoveFromCart = onRemoveFromCart;
            Owner = new RecruitVolunteerOwnerVM(owner, (int)owner.GetRelationWithPlayer());

            maxAllowedPesants = Math.Min(Jab.Nov[OwnerHero].pesants, Jab.CD[OwnerHero.Culture].ComputeMaxAllowedPesants(Hero.MainHero, OwnerHero) - (Jab.MAX_PESANT_RECRUITS - Jab.Nov[OwnerHero].pesants));
            maxAllowedNobles  = Math.Min(Jab.Nov[OwnerHero].nobles,  Jab.CD[OwnerHero.Culture].ComputeMaxAllowedNobles (Hero.MainHero, OwnerHero) - (Jab.MAX_NOBLE_RECRUITS  - Jab.Nov[OwnerHero].nobles));
            if (maxAllowedPesants < 0)
                maxAllowedPesants = 0;
            if (maxAllowedNobles < 0)
                maxAllowedNobles = 0;
            numberOfAvailablePesants =  maxAllowedPesants;
            numberOfAvailableNobles  =  maxAllowedNobles;

            JL.LogUI($"CTOR pesants {Jab.Nov[OwnerHero].pesants}, maxAllowedPesants: {maxAllowedPesants}");

            RefreshNumberOfRecruits();

            Troops = new MBBindingList<VolunteerVM>();

            List<CharacterObject> troopList = new List<CharacterObject>();

            int i = 0;
            foreach (UnitType troop in troops)
            {
                CharacterObject tempTroop = Jab.CD[OwnerHero.Culture].GetUnitFromTier(troop);

                troopList.Add(tempTroop);

                VolunteerVM volunteerVM = new VolunteerVM(this, tempTroop, i, ExecuteRecruit, ExecuteRemoveFromCart)
                {
                    CanBeRecruited = false,
                    PlayerHasEnoughRelation = (tempTroop == null)? false : true,
                    UnitTier = troop,
                };

                // JL.Log(JLT.RecruitmentUI, $"CheckCanBeRecruited, {volunteerVM.UnitTier}, {Jab.CD[OwnerHero.Culture].CanPlayerRecruitUnitTier(volunteerVM.UnitTier)}, {numberOfAvailableNobles}, {numberOfAvailablePesants}");
                // JL.Log(JLT.RecruitmentUI, $"CheckCanBeRecruited, {Campaign.Current.MainParty.EffectiveQuartermaster.GetSkillValue(DefaultSkills.Steward)}, {Campaign.Current.MainParty.EffectiveScout.GetSkillValue(DefaultSkills.Scouting)}, {Hero.MainHero.GetSkillValue(DefaultSkills.Tactics)}, {Clan.PlayerClan.Tier}");

                Troops.Add(volunteerVM);
                i++;
            }

            VolunteerTroops = troopList;

            CheckCanBeRecruited();

            RecruitHint = new HintViewModel();
            RefreshProperties();
        }

        public override void RefreshValues()
        {
            base.RefreshValues();
            RefreshProperties();
            Owner?.RefreshValues();
            Troops.ApplyActionOnAllItems(delegate (VolunteerVM x)
            {
                x.RefreshValues();
            });
        }

        public void ExecuteRecruit(VolunteerVM troop)
        {
            if (troop.CanBeRecruited)
            {
                JL.LogUI($"notable name: {OwnerHero.Name}, number of pesants: {numberOfAvailablePesants}, number of nobles: {numberOfAvailableNobles}");

                if (troop.UnitTier == UnitType.NobleUnit)
                    numberOfAvailableNobles -= 1;
                else
                    numberOfAvailablePesants -= 1;

                if (!HavePeasantsAvailable () || !HaveNoblesAvailable ())
                    CheckCanBeRecruited();
                RefreshNumberOfRecruits();

                _onRecruit(this, troop);
            }
            RefreshProperties();
        }

        public void ExecuteRemoveFromCart(VolunteerVM troop)
        {
            if (troop.UnitTier == UnitType.NobleUnit)
                numberOfAvailableNobles++;
            else
                numberOfAvailablePesants++;

            CheckCanBeRecruited();
            RefreshNumberOfRecruits();

            JL.LogUI($"troop in cart, trying to remove, pesants: {numberOfAvailablePesants}, nobles: {numberOfAvailableNobles}");

            _onRemoveFromCart(this, troop);
            RefreshProperties();
        }

        private void RefreshProperties()
        {
            RecruitText = GoldCost.ToString();
            QuantityText = GameTexts.FindText("str_none").ToString();
        }

        public void UpdateGlobalRecruitPool()
        {
            JL.LogUI($"old pesants {Jab.Nov[OwnerHero].pesants}, maxAllowedPesants: {maxAllowedPesants}, pesants left: {numberOfAvailablePesants}");
            Jab.Nov[OwnerHero].pesants -= (maxAllowedPesants - numberOfAvailablePesants);
            Jab.Nov[OwnerHero].nobles  -= (maxAllowedNobles  - numberOfAvailableNobles );
        }

        internal int GetNumberOfAvailableRecruits()
        {
            return numberOfAvailablePesants;
        }
    }
}
