﻿using TaleWorlds.SaveSystem;

namespace JusAdBellum
{
    [SaveableRootClass(42_666_420)]
    public class Volunteers
    {
        public Volunteers() 
        {
            pesants = Jab.MAX_PESANT_RECRUITS;
            nobles = 0;
        }

        public Volunteers(bool canHaveNobles) 
        {
            pesants = Jab.MAX_PESANT_RECRUITS;
            if (canHaveNobles)
                nobles = Jab.MAX_NOBLE_RECRUITS;
        }

        public override string ToString()
        {
            return $"{pesants}  |  {nobles}";
        }

        [SaveableField(1)]
        public int pesants;
        [SaveableField(2)]
        public int nobles;
    }
}
