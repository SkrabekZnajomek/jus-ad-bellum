JAB:
0.7.5
Updated code to work with 1.1.0 Version of the game.
Split mod into two. Rest of the changes can be found under there.


Units:
0.5.0
I had choosen to streamline trees and removed units which only propose was to help player.
To help players in early stages of the game now mercenary share requirements with Levy.
Nobles from other culture can be recruited if you have more than 80 relation with notable.
Volunteers were nerfed.
Empire main ranged unit is now crossbowman. Additionally moved ranged unit from levy tree to volunteer and made it an archer.
Aserai now mainly use javelins. Weakened their shield infantry, removed horse archers. Streamlined trees.
Small nerf to Khuzait horse archers.
Sturgia lost horse skirmisher. Uses primarily trowing axes.
Vlandia redundant crossbowmen removed. Updated volunteer tree to make it more useful.