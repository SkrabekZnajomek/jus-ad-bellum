﻿using HarmonyLib;
using Helpers;
using JusAdBellum.CultureUnitLists;
using TaleWorlds.CampaignSystem;
using TaleWorlds.CampaignSystem.Settlements;
using TaleWorlds.Library;

namespace JusAdBellum.Patches
{
    [HarmonyPatch(typeof(SettlementHelper))]
    public class SettlementHelpersPatch
    {
        [HarmonyPrefix]
        [HarmonyPatch("NumberOfVolunteersCanBeRecruitedForGarrison")]
        public static bool NumberOfVolunteersCanBeRecruitedForGarrisonPatch(Settlement settlement, ref int __result)
        {
			__result = NumberOfVolunteersCanBeRecruitedForGarrison2(settlement);

			return false;
        }

		public static int NumberOfVolunteersCanBeRecruitedForGarrison2(Settlement settlement)
        {
			int available = 0;
            Hero owner = settlement.OwnerClan.Leader;

			foreach (Hero notable in settlement.Notables)
			{
                available += MathF.Max(0, Jab.Nov[notable].pesants - 
                (Jab.MAX_PESANT_RECRUITS - Jab.CD[settlement.Culture].ComputeMaxAllowedPesants(owner, notable)));
			}
			foreach (Village village in settlement.BoundVillages)
			{
				if (village.VillageState == Village.VillageStates.Normal)
				{
					available += NumberOfVolunteersCanBeRecruitedForGarrison2(village.Settlement);
				}
			}

			return available;
        }

        [HarmonyPrefix]
        [HarmonyPatch("IsThereAnyVolunteerCanBeRecruitedForGarrison")]
        public static bool IsThereAnyVolunteerCanBeRecruitedForGarrisonPatch(Settlement settlement, ref bool __result)
		{
			__result = IsThereAnyVolunteerCanBeRecruitedForGarrison2(settlement);
			return false;
		}

        public static bool IsThereAnyVolunteerCanBeRecruitedForGarrison2(Settlement settlement)
		{
			Hero owner = settlement.OwnerClan.Leader;
			foreach (Hero notable in settlement.Notables)
			{
                if (MathF.Max(0, Jab.Nov[notable].pesants - 
                    (Jab.MAX_PESANT_RECRUITS - Jab.CD[settlement.Culture].ComputeMaxAllowedPesants(owner, notable))) > 0)
                {
                    return true;
                }
			}
			foreach (Village village in settlement.BoundVillages)
			{
				if (village.VillageState == Village.VillageStates.Normal)
				{
                    if (IsThereAnyVolunteerCanBeRecruitedForGarrison2(village.Settlement) == true)
                    {
                        return true;
                    }
				}
			}
			return false;
		}
    }
}
