#Short description:
A base mod that changes recruitment to what RTS and Total War have. Need another mod that provides units and info about them.

#Hi I think you would like to know that this mod by itself will break the game.
Because it is base mod for other mods in specific data is not provided it can break game.
You need to download another mod that will make use of it. My own creation is below.

##If you are returning:
Hi. I'm glad to have you back. This mod just got split into two.
You may remember that both were uploaded here this is no longer true.

###My own troop trees:
[Link]

#Actual description:
So like I got an idea if every notable have 6 buttons to choose recruit why not make use of them and turn recruitment UI into what RTS has?

Generally, this mod changes recruitment UI by removing one troop from the list and putting static troops on every field.
When you click on those troops they will be added to the cart on the right but the option to recruit this specific troop will not be removed.
Because the troops to choose always stay the same(per culture ofc.) you are effectively choosing which troop tree you want to get.
The only limiter is the number of troops notable have and is willing to give you, like in vanilla.

Additionally, there are two pools that you can draft from.
Meaning that you can use one pool to recruit from a few options and another to pull from other ones.
My troop trees have peasants and nobles. You can use nobles only to recruit base troops from the noble tree like in vanilla only with a separate number of the noble pool.

Additionally, any troop tree creator that uses this framework can change:
-List of available troops based on anything they want.
-Requirements to have every option unlocked.
-Proportions or usage of troops by lords.
 
Please check out the description of my troop tree mod If you want to know how I used the above possibilities. Link above.

##If you are a modder and got interested feel free to check out my repo and/or contact me.