﻿using HarmonyLib;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using TaleWorlds.Core;
using TaleWorlds.Library;

namespace JusAdBellum.Utility
{
    class WeightedRandom
    {
        static public int GetNumberWeighted(float[] weights)
        {
            float random = MBRandom.RandomFloat;
            for (int i = weights.Length - 1; i >= 0; i--)
            {
                if (random <= weights[i])
                {
                    return i;
                }
                else
                    random -= weights[i];
            }

            return 0;
        }
    }


    public enum JLT
    {
        Core,
        numberOfVolunteers,
        RecruitmentUI,
        AIRecruitment,
        Units,
        TutorialPatches,
        UpdateRecruits,
        Debug,
    }

    public class JL
    {

        enum LogRouting
        {
            TO_FILE,
            ON_SCREEN,
            TO_DEV_NULL
        }
        private static LogRouting RouteTo = LogRouting.TO_FILE;

        [ConditionalAttribute("DEBUG")]
        public static void LogAI(String input)
        {
            Log(JLT.AIRecruitment, input);
        }

        [ConditionalAttribute("DEBUG")]
        public static void LogCore(String input)
        {
            Log(JLT.Core, input);
        }

        public static void LogUnits(String input)
        {
            Log(JLT.Units, input);
        }

        [ConditionalAttribute("DEBUG")]
        public static void LogUI(String input)
        {
            Log(JLT.RecruitmentUI, input);
        }

        [ConditionalAttribute("DEBUG")]
        public static void LogUR(String input)
        {
            Log(JLT.UpdateRecruits, input);
        }

        [ConditionalAttribute("DEBUG")]
        public static void LogD(String input)
        {
            Log(JLT.Debug, input);
        }

        public static void Log(JLT logtype, String input)
        {
            switch (logtype)
            {
                case JLT.numberOfVolunteers:
                case JLT.Debug:
                case JLT.TutorialPatches:
                case JLT.UpdateRecruits:
                case JLT.RecruitmentUI:
                case JLT.Units:
                case JLT.Core:
                case JLT.AIRecruitment:
                    return;
                    // break;
            }

            input = logtype.ToString() + " :: " + input;

            if (RouteTo == LogRouting.TO_FILE)
                FileLog.Log(input);
            else if (RouteTo == LogRouting.ON_SCREEN)
                InformationManager.DisplayMessage(new InformationMessage(input));
        }

        [CommandLineFunctionality.CommandLineArgumentFunction("routeLogs", "jas")]
        public static string RouteLogging(List<string> args)
        {
            string result = "success";
            string arg = "";
            try
            {
                arg = args[0];
            }
            catch
            {
                result = "error no arg given";
            }

            if (!Enum.TryParse(arg, out RouteTo))
                result = $"wrong enum value {arg}";

            return result;
        }


        private JL() { }
    }
}
