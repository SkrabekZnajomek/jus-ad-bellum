﻿using HarmonyLib;
using TaleWorlds.CampaignSystem;
using TaleWorlds.CampaignSystem.CampaignBehaviors.AiBehaviors;
using TaleWorlds.CampaignSystem.Settlements;
using TaleWorlds.Library;

namespace JusAdBellum.code.Patches
{
    [HarmonyPatch(typeof(AiVisitSettlementBehavior))]
    class AiVisitSettlementBehaviorPatch
    {
        [HarmonyPrefix]
        [HarmonyPatch("ApproximateNumberOfVolunteersCanBeRecruitedFromSettlement")]
        public static bool ApproximateNumberOfVolunteersCanBeRecruitedFromSettlementPatch(Hero hero, Settlement settlement, ref int __result)
        {
            int minExpectation = 4;
			if (hero.MapFaction != settlement.MapFaction)
			{
				minExpectation = 2;
			}
			int real = 0;
			foreach (Hero notable in settlement.Notables)
			{
                real += MathF.Max(0, Jab.Nov[notable].pesants - (Jab.MAX_PESANT_RECRUITS - minExpectation));
			}
			__result = real;

            return false;
        }

    }
}
