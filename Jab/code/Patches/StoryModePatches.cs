﻿using HarmonyLib;
using JusAdBellum.Utility;
using StoryMode.GameComponents.CampaignBehaviors;
using System.Linq;
using TaleWorlds.CampaignSystem;
using TaleWorlds.CampaignSystem.Settlements;

namespace JusAdBellum.Patches
{
    [HarmonyPatch(typeof(TutorialPhaseCampaignBehavior))]
    public class StoryModePatches
    {
        [HarmonyPrefix]
        [HarmonyPatch("storymode_recruit_volunteers_on_consequence")]
        static void PrepareRecruitOptionForTutorialPatch()
        {
            JL.Log(JLT.TutorialPatches, "PrepareRecruitOptionForTutorialPrefix");

            Hero notable = Settlement.CurrentSettlement.Notables.First<Hero>();
            Jab.Nov[notable].pesants = 8;

            Jab.CD[Settlement.CurrentSettlement.Culture].PrepareTutorialUnits();
        }

        [HarmonyPostfix]
        [HarmonyPatch("OnStoryModeTutorialEnded")]
        static void OnStoryModeTutorialEndedPostfix()
        {
            Settlement settlement = Settlement.Find("village_ES3_2");

            Jab.CD[settlement.Culture].ClearTutorialUnits();

            Hero notable = settlement.Notables.First<Hero>();
            for (int i = 0; i < notable.VolunteerTypes.Length; i++)
            {
                notable.VolunteerTypes[i] = null;
            }

            JL.Log(JLT.TutorialPatches, "OnStoryModeTutorialEndedPostfix");
        }
    }
}
