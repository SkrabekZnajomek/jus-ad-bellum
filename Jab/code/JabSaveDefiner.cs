﻿using System.Collections.Generic;
using TaleWorlds.CampaignSystem;
using TaleWorlds.SaveSystem;

namespace JusAdBellum
{
    class JabSaveDefiner : SaveableTypeDefiner
    {
        public JabSaveDefiner() : base(42_666_420) { }

        protected override void DefineClassTypes()
        {
            AddClassDefinition(typeof(Volunteers), 1);
        }

        protected override void DefineContainerDefinitions()
        {
            ConstructContainerDefinition(typeof(Dictionary<Hero, Volunteers>));
        }
    }
}
