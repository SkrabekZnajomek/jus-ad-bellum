﻿using HarmonyLib;
using JusAdBellum.CultureUnitLists;
using System.Collections.Generic;
using TaleWorlds.CampaignSystem;
using TaleWorlds.CampaignSystem.CampaignBehaviors;
using TaleWorlds.CampaignSystem.Encounters;
using TaleWorlds.CampaignSystem.GameMenus;
using TaleWorlds.CampaignSystem.Party;
using TaleWorlds.CampaignSystem.Roster;
using TaleWorlds.CampaignSystem.Settlements;

namespace JusAdBellum.Patches
{
    // [HarmonyPatch(typeof(PlayerTownVisitCampaignBehavior))]
    public class PlayerTownVisitCampaignBehaviorPatches
    {
        // [HarmonyPostfix]
        // [HarmonyPatch("game_menu_village_hostile_action_force_volunteers_condition")]
        static void  game_menu_village_hostile_action_force_volunteers_condition_patch(MenuCallbackArgs args)
        {
            if (args.Tooltip != null &&
                args.Tooltip.Equals("{=7Bgs5A16}There are no recruits right now."))
            {
                bool areThereTroops = false;
                foreach (Hero notable in MobileParty.MainParty.CurrentSettlement.Notables)
                {
                    if (Jab.Nov[notable].pesants > 0 && Jab.Nov[notable].nobles > 0)
                    {
                        areThereTroops = true;
                        break;
                    }
                }
                if (areThereTroops)
                {
                    args.IsEnabled = true;
                    args.Tooltip = null;
                }
            }
        }

        // [HarmonyPrefix]
        // [HarmonyPatch("village_force_volunteers_ended_successfully_on_consequence")]
        static bool village_force_volunteers_ended_successfully_on_consequence_patch(MenuCallbackArgs args, PlayerTownVisitCampaignBehavior __instance)
        {
			args.optionLeaveType = GameMenuOption.LeaveType.Leave;
			GameMenu.SwitchToMenu("village");
			TroopRoster troopRoster = TroopRoster.CreateDummyTroopRoster();
			foreach (Hero notable in MobileParty.MainParty.CurrentSettlement.Notables)
			{
				int nobles = Jab.Nov[notable].nobles;
				int maxVolunteerNumber = 3;

				for (int i = 0; i < nobles; i++)
				{
					troopRoster.AddToCounts(Jab.CD[notable.Culture].GetUnitFromTier(UnitType.NobleUnit), 
											1, false, 0, 0, true, -1);
					Jab.Nov[notable].nobles--;
					maxVolunteerNumber--;
				}
				while (maxVolunteerNumber > 0 && Jab.Nov[notable].pesants > 0)
				{
					troopRoster.AddToCounts(Jab.CD[notable.Culture].GetUnitFromTier(UnitType.Regular),
											1, false, 0, 0, true, -1);
					Jab.Nov[notable].pesants--;
					maxVolunteerNumber--;
				}
			}

			Dictionary<string, CampaignTime> dictionary;

			System.Reflection.FieldInfo receivedObject = typeof(PlayerTownVisitCampaignBehavior)
				.GetField("_villageLastHostileActionTimeDictionary", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance);

			dictionary = (Dictionary<string, CampaignTime>)receivedObject.GetValue(__instance);
			if (!dictionary.ContainsKey(Settlement.CurrentSettlement.StringId))
			{
				dictionary.Add(Settlement.CurrentSettlement.StringId, CampaignTime.Now);
			}
			else
			{
				dictionary[Settlement.CurrentSettlement.StringId] = CampaignTime.Now;
			}
			receivedObject.SetValue(__instance, dictionary);

			PartyScreenManager.OpenScreenAsLoot(troopRoster, TroopRoster.CreateDummyTroopRoster(), MobileParty.MainParty.CurrentSettlement.Name, troopRoster.TotalManCount, null);
			PlayerEncounter.Current.ForceVolunteers = false;
			PlayerEncounter.Current.FinalizeBattle();

			return false;
		}
    }
}
