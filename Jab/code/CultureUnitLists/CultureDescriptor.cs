﻿using JusAdBellum.Utility;
using System.Collections.Generic;
using System.Linq;
using TaleWorlds.CampaignSystem;
using TaleWorlds.CampaignSystem.CharacterDevelopment;
using TaleWorlds.CampaignSystem.Party;
using TaleWorlds.Core;
using TaleWorlds.ObjectSystem;

namespace JusAdBellum.CultureUnitLists
{
    public enum UnitType
    {
        Invalid = -1,
        Unit0 = 0,
        Unit1 = 1,
        Unit2 = 2,
        Unit3 = 3,
        Unit4 = 4,
        Unit5 = 5,
        Unit6 = 6,
        Unit7 = 7,
        Unit8 = 8,
        NobleUnit = 9,
        Volunteer = Unit0,
        Levy = Unit1,
        Regular = Unit2,
        Elite = Unit3,
        Mercenary = Unit4,
        Additional1 = Unit5,
        Additional2 = Unit6,
        Additional3 = Unit7,
        Additional4 = Unit8,
    }

    public abstract class CultureDescriptor
    {
        public string CultureName { get; protected set; } = "";

        protected Dictionary<UnitType, string> UnitIDsDictionary = new Dictionary<UnitType, string>();

        protected Dictionary<UnitType, CharacterObject> Units = new Dictionary<UnitType, CharacterObject>();

        protected CultureObject Culture = null;

        public virtual void ClearTutorialUnits()
        { }

        public virtual void PrepareTutorialUnits()
        { }

        public virtual CharacterObject GetRecruitForNPCParty(MobileParty party, Hero notable, bool isTown)
        {
            int freespace = party.LimitedPartySize - party.Party.NumberOfAllMembers;
            CharacterObject newRecruit = null;

            JL.LogAI($"NPC {party.LeaderHero.Name} recruit from notable: {notable.Name}, CanNPCRecruitNobleUnits: {CanNPCRecruitNobleUnits(party.LeaderHero, notable)}, nobles: {Jab.Nov[notable].nobles}");

            if (CanNPCRecruitNobleUnits(party.LeaderHero, notable) && Jab.Nov[notable].nobles > 0 && !isTown)
            {
                if (party.LeaderHero.Gold > Campaign.Current.Models.PartyWageModel.GetTroopRecruitmentCost(Units[UnitType.NobleUnit], party.LeaderHero))
                    newRecruit = Units[UnitType.NobleUnit];

                if (newRecruit != null)
                    Jab.Nov[notable].nobles -= 1;
            }
            else if (Jab.Nov[notable].pesants > 0)
            {
                if (party.ActualClan.Culture != notable.Culture ||
                    party.ActualClan.Tier < 4 && isTown && freespace < 20)
                {
                    newRecruit = Units[UnitType.Mercenary];
                }
                else
                {
                    float[] disposition = GetDispositionForClan(party.ActualClan.Tier, party.ActualClan.Culture);
                    UnitType choosenTier = (UnitType)WeightedRandom.GetNumberWeighted(disposition);
                    newRecruit = GetUnitFromTier(choosenTier);
                }

                if (newRecruit != null)
                    Jab.Nov[notable].pesants -= 1;
            }

            return newRecruit;
        }

         public virtual int ComputeMaxAllowedPesants(Hero buyer, Hero seller)
        {
            int basic = 2;
            int dificultyBonus = Campaign.Current.Models.DifficultyModel.GetPlayerRecruitSlotBonus();
            int sameFaction = (seller.CurrentSettlement != null && buyer.MapFaction == seller.CurrentSettlement.MapFaction) ? 1 : 0;
            int isAtWar = (seller.CurrentSettlement != null && buyer.MapFaction.IsAtWarWith(seller.CurrentSettlement.MapFaction)) ? (-2) : 0;
            if (buyer.IsMinorFactionHero && seller.CurrentSettlement != null && seller.CurrentSettlement.IsVillage)
			{
				isAtWar = 0;
			}
            int relation = buyer.GetRelation(seller);
            int relationBonus = ((relation >= 60) ? 4 : ((relation >= 40) ? 3 : ((relation >= 25) ? 2 : ((relation >= 10) ? 1 : ((relation < 0) ? (-2) : 0)))));
            int perkBonus = 0;
            if (seller.IsGangLeader &&
                seller.CurrentSettlement != null &&
                seller.CurrentSettlement.OwnerClan == buyer.Clan &&
                ((seller.CurrentSettlement.IsTown &&
                    (seller.CurrentSettlement.Town.Governor?.GetPerkValue(DefaultPerks.Roguery.OneOfTheFamily) ?? false)) ||
                    (seller.CurrentSettlement.IsVillage &&
                    (seller.CurrentSettlement.Village.Bound.Town.Governor?.GetPerkValue(DefaultPerks.Roguery.OneOfTheFamily) ??
                    false))))
            {
                perkBonus += (int)DefaultPerks.Roguery.OneOfTheFamily.SecondaryBonus;
            }

            if (seller.IsMerchant && buyer.GetPerkValue(DefaultPerks.Trade.ArtisanCommunity))
            {
                perkBonus += (int)DefaultPerks.Trade.ArtisanCommunity.SecondaryBonus;
            }

            if (seller.Culture == buyer.Culture && buyer.GetPerkValue(DefaultPerks.Leadership.CombatTips))
            {
                perkBonus += (int)DefaultPerks.Leadership.CombatTips.SecondaryBonus;
            }

            if (seller.IsRuralNotable && buyer.GetPerkValue(DefaultPerks.Charm.Firebrand))
            {
                perkBonus += (int)DefaultPerks.Charm.Firebrand.SecondaryBonus;
            }

            if (seller.IsUrbanNotable && buyer.GetPerkValue(DefaultPerks.Charm.FlexibleEthics))
            {
                perkBonus += (int)DefaultPerks.Charm.FlexibleEthics.SecondaryBonus;
            }

            if (buyer.PartyBelongedTo != null && buyer.PartyBelongedTo.EffectiveEngineer != null && buyer.PartyBelongedTo.EffectiveEngineer.GetPerkValue(DefaultPerks.Engineering.EngineeringGuilds))
            {
                perkBonus += (int)DefaultPerks.Engineering.EngineeringGuilds.PrimaryBonus;
            }

            int allowed = basic + relationBonus + dificultyBonus + sameFaction + isAtWar + perkBonus;

            if (allowed > Jab.MAX_PESANT_RECRUITS)
                allowed = Jab.MAX_PESANT_RECRUITS;
            else if (allowed < 0)
                allowed = 0;

            JL.LogUI($"computeMaxAllowedPesants: basic: {basic} + relationBonus: {relationBonus} + dificultyBonus: {dificultyBonus} + sameFaction: {sameFaction} + isAtWar: {isAtWar} + perkBonus: {perkBonus} = allowed: {allowed}");
            return allowed;
        }

        public virtual int ComputeMaxAllowedNobles(Hero buyer, Hero seller)
        {
            int dificultyBonus = Campaign.Current.Models.DifficultyModel.GetPlayerRecruitSlotBonus();
            int sameFaction = (seller.CurrentSettlement != null && buyer.MapFaction == seller.CurrentSettlement.MapFaction) ? 3 : (-2);
            int isAtWar = (seller.CurrentSettlement != null && buyer.MapFaction.IsAtWarWith(seller.CurrentSettlement.MapFaction)) ? (-4) : 0;
            int relation = (buyer.Culture != seller.Culture)? buyer.GetRelation(seller) - 50 : buyer.GetRelation(seller);
            int relationBonus = ((relation >= 60) ? 4 : ((relation >= 40) ? 3 : ((relation >= 25) ? 2 : ((relation >= 10) ? 1 : ((relation < 0) ? (-2) : 0)))));

            int allowed = relationBonus + dificultyBonus + sameFaction + isAtWar;

            if (allowed > Jab.MAX_NOBLE_RECRUITS)
                allowed = Jab.MAX_NOBLE_RECRUITS;
            else if (allowed < 0)
                allowed = 0;

            JL.LogUI($"ComputeMaxAllowedNobles: relationBonus: {relationBonus} + dificultyBonus: {dificultyBonus} + sameFaction: {sameFaction} + isAtWar: {isAtWar} = allowed: {allowed}");
            return allowed;
        }

        public virtual CharacterObject GetRecruitForTown()
        {
            float unitPicker = MBRandom.RandomFloat % 1;

            if (unitPicker < 0.35)
                return Units[UnitType.Levy];
            else
                return Units[UnitType.Regular];
        }

        public virtual bool CanNPCRecruitNobleUnits(Hero lord, Hero notable)
        {
            if (lord.Clan == Clan.PlayerClan)
                return CanPlayerRecruitNoble(notable);
            else if ((lord.Clan.Tier > 3 && lord.Clan.Culture == notable.Culture)
                    || lord.Clan.Tier > 5)
                return true;
            return false;
        }

        public virtual bool CanPlayerRecruitMercenary(Hero notable)
        {
            return Campaign.Current.MainParty.EffectiveQuartermaster.GetSkillValue(DefaultSkills.Steward) > 50 &&
                    Campaign.Current.MainParty.EffectiveScout.GetSkillValue(DefaultSkills.Scouting) > 10 &&
                    (
                        Hero.MainHero.GetSkillValue(DefaultSkills.Tactics) > 10 ||
                        Hero.MainHero.GetSkillValue(DefaultSkills.Leadership) > 10 ||
                        (Campaign.Current.MainParty.EffectiveScout.GetSkillValue(DefaultSkills.Medicine) > 20 &&
                        Campaign.Current.MainParty.EffectiveScout.GetSkillValue(DefaultSkills.Engineering) > 20)
                    ) &&
                    Clan.PlayerClan.Tier > 1;
        }

        public virtual bool CanPlayerRecruitTier3(Hero notable)
        {
            return Campaign.Current.MainParty.EffectiveQuartermaster.GetSkillValue(DefaultSkills.Steward) > 200 &&
                    Campaign.Current.MainParty.EffectiveScout.GetSkillValue(DefaultSkills.Scouting) > 50 &&
                    (
                        Hero.MainHero.GetSkillValue(DefaultSkills.Tactics) > 50 ||
                        Hero.MainHero.GetSkillValue(DefaultSkills.Leadership) > 70 ||
                        (Campaign.Current.MainParty.EffectiveScout.GetSkillValue(DefaultSkills.Medicine) > 80 &&
                        Campaign.Current.MainParty.EffectiveScout.GetSkillValue(DefaultSkills.Engineering) > 50)
                    ) &&
                    Clan.PlayerClan.Tier > 4;
        }

        public virtual bool CanPlayerRecruitNoble(Hero notable)
        {
            return Campaign.Current.MainParty.EffectiveQuartermaster.GetSkillValue(DefaultSkills.Steward) > 150 &&
                    Campaign.Current.MainParty.EffectiveScout.GetSkillValue(DefaultSkills.Scouting) > 35 &&
                    (
                        Hero.MainHero.GetSkillValue(DefaultSkills.Tactics) > 40 ||
                        Hero.MainHero.GetSkillValue(DefaultSkills.Leadership) > 50 ||
                        (Campaign.Current.MainParty.EffectiveScout.GetSkillValue(DefaultSkills.Medicine) > 50 &&
                        Campaign.Current.MainParty.EffectiveScout.GetSkillValue(DefaultSkills.Engineering) > 50)
                    ) &&
                    Clan.PlayerClan.Tier > 3;
        }

        public virtual bool CanPlayerRecruitTier2(Hero notable)
        {
            return Campaign.Current.MainParty.EffectiveQuartermaster.GetSkillValue(DefaultSkills.Steward) > 100 &&
                    Campaign.Current.MainParty.EffectiveScout.GetSkillValue(DefaultSkills.Scouting) > 20 &&
                    (
                        Hero.MainHero.GetSkillValue(DefaultSkills.Tactics) > 30 ||
                        Hero.MainHero.GetSkillValue(DefaultSkills.Leadership) > 30 ||
                        (Campaign.Current.MainParty.EffectiveScout.GetSkillValue(DefaultSkills.Medicine) > 40 &&
                        Campaign.Current.MainParty.EffectiveScout.GetSkillValue(DefaultSkills.Engineering) > 30)
                    ) &&
                    Clan.PlayerClan.Tier > 2;
        }

        public virtual bool CanPlayerRecruitTier1(Hero notable)
        {
            return Campaign.Current.MainParty.EffectiveQuartermaster.GetSkillValue(DefaultSkills.Steward) > 50 &&
                    Campaign.Current.MainParty.EffectiveScout.GetSkillValue(DefaultSkills.Scouting) > 10 &&
                    (
                        Hero.MainHero.GetSkillValue(DefaultSkills.Tactics) > 10 ||
                        Hero.MainHero.GetSkillValue(DefaultSkills.Leadership) > 10 ||
                        (Campaign.Current.MainParty.EffectiveScout.GetSkillValue(DefaultSkills.Medicine) > 20 &&
                        Campaign.Current.MainParty.EffectiveScout.GetSkillValue(DefaultSkills.Engineering) > 20)
                    ) &&
                    Clan.PlayerClan.Tier > 1;
        }

        public virtual bool CanPlayerRecruitVulunteers()
        {
            return true;
        }

        public virtual bool CanPlayerRecruitUnitTier(UnitType tier, Hero notable)
        {
            bool canHe = false;
            switch (tier)
            {
                case UnitType.Volunteer:
                    canHe = CanPlayerRecruitVulunteers();
                    break;
                case UnitType.Levy:
                    canHe = CanPlayerRecruitTier1(notable);
                    break;
                case UnitType.Regular:
                    canHe = CanPlayerRecruitTier2(notable);
                    break;
                case UnitType.Elite:
                    canHe = CanPlayerRecruitTier3(notable);
                    break;
                case UnitType.NobleUnit:
                    canHe = CanPlayerRecruitNoble(notable);
                    break;
                default:
                    canHe = CanPlayerRecruitMercenary(notable);
                    break;
            }

            return canHe;
        }

        public virtual CharacterObject GetUnitFromTier(UnitType tier)
        {
            return Units[tier];
        }

        public virtual void SetRequirments(UnitType tier, Hero notable)
        {
            if (notable.Culture != Hero.MainHero.Culture
                && tier == UnitType.NobleUnit)
            {
                GameTexts.SetVariable("QUAT_REQ", 150);
                GameTexts.SetVariable("SCOUT_REQ", 35);
                GameTexts.SetVariable("TACT_REQ", 40);
                GameTexts.SetVariable("LEAD_REQ", 50);
                GameTexts.SetVariable("REP_REQ", 50);
                GameTexts.SetVariable("SURG_REQ", 50);
                GameTexts.SetVariable("ENGI_REQ", 40);
                GameTexts.SetVariable("TIER_REQ", 4);
            }
            switch (tier)
            {
                case UnitType.Volunteer:
                    GameTexts.SetVariable("QUAT_REQ", 0);
                    GameTexts.SetVariable("SCOUT_REQ", 0);
                    GameTexts.SetVariable("TACT_REQ", 0);
                    GameTexts.SetVariable("LEAD_REQ", 0);
                    GameTexts.SetVariable("REP_REQ", 0);
                    GameTexts.SetVariable("SURG_REQ", 0);
                    GameTexts.SetVariable("ENGI_REQ", 0);
                    GameTexts.SetVariable("TIER_REQ", 0);
                    break;
                case UnitType.Levy:
                    GameTexts.SetVariable("QUAT_REQ", 50);
                    GameTexts.SetVariable("SCOUT_REQ", 10);
                    GameTexts.SetVariable("TACT_REQ", 10);
                    GameTexts.SetVariable("LEAD_REQ", 10);
                    GameTexts.SetVariable("REP_REQ", 0);
                    GameTexts.SetVariable("SURG_REQ", 20);
                    GameTexts.SetVariable("ENGI_REQ", 20);
                    GameTexts.SetVariable("TIER_REQ", 2);
                    break;
                case UnitType.Mercenary:
                case UnitType.Regular:
                    GameTexts.SetVariable("QUAT_REQ", 100);
                    GameTexts.SetVariable("SCOUT_REQ", 20);
                    GameTexts.SetVariable("TACT_REQ", 30);
                    GameTexts.SetVariable("LEAD_REQ", 30);
                    GameTexts.SetVariable("REP_REQ", 0);
                    GameTexts.SetVariable("SURG_REQ", 40);
                    GameTexts.SetVariable("ENGI_REQ", 30);
                    GameTexts.SetVariable("TIER_REQ", 3);
                    break;
                case UnitType.NobleUnit:
                    GameTexts.SetVariable("QUAT_REQ", 150);
                    GameTexts.SetVariable("SCOUT_REQ", 35);
                    GameTexts.SetVariable("TACT_REQ", 40);
                    GameTexts.SetVariable("LEAD_REQ", 50);
                    GameTexts.SetVariable("REP_REQ", 0);
                    GameTexts.SetVariable("SURG_REQ", 50);
                    GameTexts.SetVariable("ENGI_REQ", 40);
                    GameTexts.SetVariable("TIER_REQ", 4);
                    break;
                case UnitType.Elite:
                    GameTexts.SetVariable("QUAT_REQ", 200);
                    GameTexts.SetVariable("SCOUT_REQ", 50);
                    GameTexts.SetVariable("TACT_REQ", 50);
                    GameTexts.SetVariable("LEAD_REQ", 70);
                    GameTexts.SetVariable("REP_REQ", 0);
                    GameTexts.SetVariable("SURG_REQ", 80);
                    GameTexts.SetVariable("ENGI_REQ", 50);
                    GameTexts.SetVariable("TIER_REQ", 5);
                    break;
                default:
                    GameTexts.SetVariable("QUAT_REQ", 100);
                    GameTexts.SetVariable("SCOUT_REQ", 20);
                    GameTexts.SetVariable("TACT_REQ", 30);
                    GameTexts.SetVariable("LEAD_REQ", 30);
                    GameTexts.SetVariable("REP_REQ", 0);
                    GameTexts.SetVariable("SURG_REQ", 40);
                    GameTexts.SetVariable("ENGI_REQ", 30);
                    GameTexts.SetVariable("TIER_REQ", 3);
                    break;
            }

        }

        public virtual float[] GetDispositionForClan(int clanTier, CultureObject culture)
        {

            if (culture != Culture)
                return new float[6] { 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f };
            switch (clanTier)
            {
                case 2:
                case 3:
                    return new float[6] { 0.2f, 0.8f, 0.0f, 0.0f, 0.0f, 0.0f };
                case 4:
                    return new float[6] { 0.0f, 0.3f, 0.7f, 0.0f, 0.0f, 0.0f };
                case 5:
                    return new float[6] { 0.0f, 0.4f, 0.6f, 0.0f, 0.0f, 0.0f };
                case 6:
                case 7:
                    return new float[6] { 0.0f, 0.2f, 0.5f, 0.3f, 0.0f, 0.0f };
            }

            return new float[6] { 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f };
        }

        public virtual List<UnitType> GetTroopListForPlayer()
        {
            List<UnitType> trooplist;
            if (Clan.PlayerClan.Culture != Culture)
            {
                trooplist = new List<UnitType>
                {
                    UnitType.Volunteer,
                    UnitType.Mercenary,
                    UnitType.Additional1,
                    UnitType.NobleUnit,
                    UnitType.Additional3,
                };
            }
            else
            {
                trooplist = new List<UnitType>
                {
                   UnitType.Volunteer,
                   UnitType.Levy,
                   UnitType.Regular,
                   UnitType.NobleUnit,
                   UnitType.Elite,
                };
            }

            return trooplist;
        }


        public virtual void BuildCharacterList()
        {
            MBObjectManager objectManager = MBObjectManager.Instance;

            Culture = objectManager.GetObjectTypeList<CultureObject>().ToList().Find(x => x.Name.ToString() == CultureName);

            JL.LogUnits($"Bulding troop list for culture {Culture.Name}");

            var itr = UnitIDsDictionary.GetEnumerator();
            while (itr.MoveNext())
            {
                UnitType unitID = itr.Current.Key;
                CharacterObject character = objectManager.GetObject<CharacterObject>(itr.Current.Value);
                if (character == null)
                    JL.LogUnits($"Could not find unit {itr.Current.Value}, for tier {unitID}");
                else
                    JL.LogUnits($"Found unit {itr.Current.Value}, for tier {unitID}");

                Units.Add(unitID, character);
            }

            for (int i = -1; i < 10; i++)
            {
                UnitType unitID = (UnitType) i;

                if (! UnitIDsDictionary.ContainsKey(unitID))
                {
                    JL.LogUnits($"Filling null for tier {unitID}");
                    Units.Add(unitID, null);
                }
            }

            typeof(CultureObject).GetProperty("BasicTroop").DeclaringType.GetProperty("BasicTroop").
                GetSetMethod(true).Invoke(Culture, new object[] { GetBaseTroop() });
        }

        public virtual void ClearUnits()
        {
            Units.Clear();
        }

        protected void AddUnitToPersistentDictionary(UnitType type, string xmlUnitID)
        {
            UnitIDsDictionary.Add(type, xmlUnitID);
        }

        protected abstract CharacterObject GetBaseTroop();
    }
}
