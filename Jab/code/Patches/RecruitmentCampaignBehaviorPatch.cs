﻿using HarmonyLib;
using JusAdBellum.Utility;
using System.Reflection;
using TaleWorlds.CampaignSystem;
using TaleWorlds.CampaignSystem.CampaignBehaviors;
using TaleWorlds.CampaignSystem.Party;
using TaleWorlds.CampaignSystem.Settlements;
using TaleWorlds.Core;
using TaleWorlds.Library;

namespace JusAdBellum.Patches
{
    [HarmonyPatch(typeof(RecruitmentCampaignBehavior))]
    public class RecruitmentCampaignBehaviorPatch
    {
        [HarmonyPrefix]
        [HarmonyPatch("UpdateVolunteersOfNotablesInSettlement")]
        static bool UpdateVolunteersOfNotablesInSettlementPrefix(Settlement settlement)
        {
            JL.LogUR($"Next fill of volunteers started");
            if ((settlement.IsTown && !settlement.Town.InRebelliousState) ||
                (settlement.IsVillage && !settlement.Village.Bound.Town.InRebelliousState))
            {
                foreach (Hero notable in settlement.Notables)
                {
                    if (notable.CanHaveRecruits)
                    {
                        JL.LogUR($"settlement: {settlement.Name} notable: {notable.Name}");
                        var maxProduction = (0.5f + (MBRandom.RandomFloat * 2f)) % 2.0f;
                        bool canHaveNobles = Campaign.Current.Models.VolunteerModel.GetBasicVolunteer(notable) == notable.Culture.EliteBasicTroop;
                        JL.LogUR($"canHaveNobles: {canHaveNobles}, max production: {maxProduction}");
                        float production = 0.0f;

                        int i = 0;
                        for (i = Jab.Nov[notable].pesants; i < Jab.MAX_PESANT_RECRUITS; i++)
                        {
                            int index = (i > 5) ? 5 : i;
                            production += Campaign.Current.Models.VolunteerModel.GetDailyVolunteerProductionProbability(notable, index, settlement);
                            JL.LogUR($"probability factor: {production}");
                            if (production > maxProduction)
                            {
                                break;
                            }
                        }
                        Jab.Nov[notable].pesants = i;
                        JL.LogUR($"added {i} pesants, current number {Jab.Nov[notable].pesants}");

                        if (canHaveNobles && Jab.Nov[notable].nobles < Jab.MAX_NOBLE_RECRUITS && i > 1)
                        {
                            for (i = Jab.Nov[notable].nobles; i < Jab.MAX_NOBLE_RECRUITS; i++)
                            {
                                if (MBRandom.RandomFloat < 0.2f)
                                    break;
                            }
                            Jab.Nov[notable].nobles = i;
                            JL.LogUR($"added noble: {Jab.Nov[notable].nobles}");
                        }
                        // JL.LogUR($"settlement: {settlement.Name}, notable: {notable.Name}, hash: {notable.GetHashCode()}, pesants: {Jab.Nov[notable].pesants}, nobles: {Jab.Nov[notable].nobles}");
                    }
                }
            }
            return false;
		}

        [HarmonyPrefix]
        [HarmonyPatch("RecruitVolunteersFromNotable")]
        static bool RecruitVolunteersFromNotablePatch(RecruitmentCampaignBehavior __instance, MobileParty mobileParty, Settlement settlement)
        {
            if ((((float)mobileParty.Party.NumberOfAllMembers + 0.5f) / (float)mobileParty.LimitedPartySize) >= 1f)
            {
                return false;
            }

            foreach (Hero notable in settlement.Notables)
            {
                if (mobileParty.IsWageLimitExceeded())
                {
                    break;
                }
                JL.LogAI($"NPC {mobileParty.LeaderHero.Name} start recruit from notable: {notable.Name}, pesants at start: {Jab.Nov[notable].pesants}, nobles at start: {Jab.Nov[notable].nobles}");

                int maxRecruits = Jab.CD[settlement.Culture].ComputeMaxAllowedPesants(
                    mobileParty.IsGarrison ? mobileParty.Party.Owner : mobileParty.LeaderHero,
                    notable);
                for (int i = 0; i < maxRecruits; i++)
                {
                    int allowance = (mobileParty.LeaderHero != null) ? ((int)MathF.Sqrt((float)mobileParty.LeaderHero.Gold / 10000f)) : 0;
                    float recruitmentChance = MBRandom.RandomFloat;

                    for (int j = 0; j < allowance; j++)
                    {
                        float randomFloat = MBRandom.RandomFloat;
                        if (randomFloat > recruitmentChance)
                        {
                            recruitmentChance = randomFloat;
                        }
                    }

                    if (mobileParty.Army != null)
                    {
                        float y = (mobileParty.Army.LeaderParty == mobileParty) ? 0.5f : 0.67f;
                        recruitmentChance = MathF.Pow(recruitmentChance, y);
                    }

                    float procentageOfFullnes = (float)mobileParty.Party.NumberOfAllMembers / (float)mobileParty.LimitedPartySize;


                    // JL.LogAI($"NPC {mobileParty.LeaderHero.Name} maxRecruits: {maxRecruits}, allowance: {allowance}, procentageOfFullnes: {procentageOfFullnes}");
                    if (recruitmentChance > procentageOfFullnes - 0.1f)
                    {
                        CharacterObject newRecruit = Jab.CD[settlement.Culture].GetRecruitForNPCParty(mobileParty, notable, false);
                        if (newRecruit != null && mobileParty.LeaderHero.Gold > Campaign.Current.Models.PartyWageModel.GetTroopRecruitmentCost(newRecruit, mobileParty.LeaderHero) && mobileParty.PaymentLimit >= mobileParty.TotalWage + Campaign.Current.Models.PartyWageModel.GetCharacterWage(newRecruit))
                        {
                            typeof(RecruitmentCampaignBehavior).GetMethod("GetRecruitVolunteerFromIndividual", BindingFlags.NonPublic | BindingFlags.Instance)
                                    .Invoke(__instance, new object[] { mobileParty, newRecruit, notable, 0 });
                            break;
                        }
                    }
                }
                JL.LogAI($"NPC {mobileParty.LeaderHero.Name} recruited from notable: {notable.Name}, pesants left: {Jab.Nov[notable].pesants}, nobles left: {Jab.Nov[notable].nobles}");
            }

            return false;
        }
    }
}
