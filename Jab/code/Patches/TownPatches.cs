﻿using HarmonyLib;
using Helpers;
using JusAdBellum.Utility;
using System;
using System.Reflection;
using TaleWorlds.CampaignSystem;
using TaleWorlds.CampaignSystem.Settlements;
using TaleWorlds.Core;
using TaleWorlds.Library;

namespace JusAdBellum.Patches
{
    [HarmonyPatch(typeof(Town))]
    public class TownPatches
    {
        [HarmonyPrefix]
        [HarmonyPatch("DailyGarrisonAdjustment")]
        static bool DailyGarrisonAdjustmentPatch(Town __instance)
        {
            MethodBase DesertOneTroopFromGarrisonMethod = typeof(Town).GetMethod("DesertOneTroopFromGarrison", BindingFlags.NonPublic | BindingFlags.Instance);

            int garrisonChange = __instance.GarrisonParty.CurrentSettlement.Town.GarrisonChange;
            int garrisonChangeAutoRecruitment = __instance.GarrisonAutoRecruitmentIsEnabled ? __instance.GarrisonParty.CurrentSettlement.Town.GarrisonChangeAutoRecruitment : 0;
            garrisonChange -= garrisonChangeAutoRecruitment;
            int partySizeLimit = __instance.GarrisonParty.LimitedPartySize;
            if (garrisonChange > 0)
            {
                garrisonChange = MBMath.ClampInt(garrisonChange, 0, partySizeLimit - __instance.GarrisonParty.Party.NumberOfAllMembers - garrisonChangeAutoRecruitment);
            }
            if (garrisonChange < 0)
            {
                for (int i = 0; i < Math.Abs(garrisonChange); i++)
                {
                    DesertOneTroopFromGarrisonMethod.Invoke(__instance, new object[] { });
                    // JL.LogAI($"Deserted one troop form garnison {__instance.Name}");
                }
            }
            else if (garrisonChange > 0)
            {
                __instance.GarrisonParty.MemberRoster.AddToCounts( Jab.CD[__instance.GarrisonParty.MapFaction.Culture].GetRecruitForTown(), garrisonChange);
            }

            if (garrisonChangeAutoRecruitment > 0)
            {
                Hero leader = __instance.GarrisonParty.CurrentSettlement.OwnerClan.Leader;
                int numberOfAvaiableRecruits = SettlementHelper.NumberOfVolunteersCanBeRecruitedForGarrison(__instance.GarrisonParty.CurrentSettlement);
                if (numberOfAvaiableRecruits > 0)
                {
                    float numberOfWantedRecruits = MBRandom.RandomFloat * (float)numberOfAvaiableRecruits;
                    foreach (Hero notable in __instance.GarrisonParty.CurrentSettlement.Notables)
                    {
                        if (numberOfWantedRecruits <= 0f)
                        {
                            break;
                        }
                        numberOfWantedRecruits -= 1f;
                        if (Jab.Nov[notable].pesants > 0)
                        {
                            int maxRecruits = Jab.CD[__instance.Culture].ComputeMaxAllowedPesants(leader, notable);
                            for (int i = 0; i < maxRecruits; i++)
                            {
                                CharacterObject recruit = Jab.CD[notable.Culture].GetRecruitForTown();
                                __instance.GarrisonParty.MemberRoster.AddToCounts(recruit, 1);
                                leader.Clan.AutoRecruitmentExpenses += Campaign.Current.Models.PartyWageModel.GetTroopRecruitmentCost(recruit, leader);
                                Jab.Nov[notable].pesants--;
                                JL.LogAI($"Added one troop to garnison {__instance.Name}, notable: {notable.Name}, pesants: {Jab.Nov[notable].pesants}");
                                break;
                            }
                        }
                    }

                    if (numberOfWantedRecruits > 0f)
                    {
                        foreach (Village boundVillage2 in __instance.GarrisonParty.CurrentSettlement.BoundVillages)
                        {
                            if (numberOfWantedRecruits <= 0f)
                            {
                                break;
                            }
                            if (boundVillage2.VillageState == Village.VillageStates.Normal)
                            {
                                foreach (Hero notable in boundVillage2.Settlement.Notables)
                                {
                                    if (numberOfWantedRecruits <= 0f)
                                    {
                                        break;
                                    }
                                    numberOfWantedRecruits -= 1f;
                                    if (Jab.Nov[notable].pesants > 0)
                                    {
                                        int maxRecruits = Jab.CD[__instance.Culture].ComputeMaxAllowedPesants(leader, notable);
                                        for (int i = 0; i < maxRecruits; i++)
                                        {
                                            CharacterObject recruit = Jab.CD[notable.Culture].GetRecruitForTown();
                                            __instance.GarrisonParty.MemberRoster.AddToCounts(recruit, 1);
                                            leader.Clan.AutoRecruitmentExpenses += Campaign.Current.Models.PartyWageModel.GetTroopRecruitmentCost(recruit, leader);
                                            Jab.Nov[notable].pesants--;
                                            JL.LogAI($"Added one troop to garnison {__instance.Name}, notable: {notable.Name}, pesants: {Jab.Nov[notable].pesants}");
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            if (__instance.GarrisonParty.Party.NumberOfAllMembers > partySizeLimit)
            {
                int num5 = MBRandom.RoundRandomized((float)(__instance.GarrisonParty.Party.NumberOfAllMembers - partySizeLimit) * 0.2f);
                for (int l = 0; l < num5; l++)
                {
                    DesertOneTroopFromGarrisonMethod.Invoke(__instance, new object[] { });
                    // JL.LogAI($"Deserted one troop form garnison {__instance.Name}");
                }
            }

            return false;
        }
    }
}
