﻿using HarmonyLib;
using JusAdBellum.CultureUnitLists;
using TaleWorlds.CampaignSystem;
using TaleWorlds.CampaignSystem.CampaignBehaviors;
using TaleWorlds.Core;

namespace JusAdBellum.Patches
{
    [HarmonyPatch(typeof(CraftingCampaignBehavior))]
    class CraftingCampaignBehaviorPatches
    {
        [HarmonyPrefix]
        [HarmonyPatch("GiveTroopToNobleAtWeaponTier")]
        static bool GiveTroopToNobleAtWeaponTierPatch(int tier, Hero noble)
        {
            CharacterObject characterObject = Jab.CD[noble.Culture].GetUnitFromTier(UnitType.Regular);
            for (int i = characterObject.Tier; i < tier; i++)
            {
                if (characterObject.UpgradeTargets == null || characterObject.UpgradeTargets.IsEmpty())
                    break;
                characterObject = characterObject.UpgradeTargets.GetRandomElement<CharacterObject>();
            }
            noble.PartyBelongedTo.AddElementToMemberRoster(characterObject, 1);

            return false;
        }
    }
}
