#Short description:
Troop tress for framework "Jus Ad Bellum". Focused on creating progression by splitting giant troops into smaller ones. And locking players from access to the best troop trees, until requirements are met.

#This mod does nothing on its own. You need to install "Jus Ad Bellum" and place it above in load order.
You may also want to read what it does, and don't forget to return and read the rest of the description :).
[Link]

#Introduction
When playing the base game I saw a few problems with the way the player is recruiting and upgrading units in the base game.
In just a few minutes you can have units with better eq than you.
A few battles later and a lot of looters dead your units will transform into living tanks seemingly without a reason.
By traveling around the world you can create a super army from the best units of every culture.
All troop trees of cultures are similar. Whereas Warband and real-life saw armies focused on one troop type.
Cultural choice has no impact on troop trees but it should have since the culture in which you grow up makes you biased toward stuff.
NPCs have a problem upgrading and conserving troops and are dragging armies of peasants around.

After some programming and testing, I present you Jus Add Bellum.

#Description
To fully understand what this mod does I recommend going to the framework page and have a read its description then return here.

##Main changes
Now every culture has a few troop trees, that differ in skill and equipment.
But at the beginning you are locked out of the better of them. You can unlock them as you progress.
Settlements with a different culture will give you limited recruitment options.
Noble troops have their recruitment pool. Meaning you can recruit a set number of them independently from others.

That's it. I will get into more detail about every change lower.

##Types of unit trees. (And army building.)

The main component of every army should be regular troops with support units fulfilling jobs that regulars cannot.

Types available from the weakest(with one exception):
Volunteers: You can think of them as strangers that you ask for protection in return for some quick buck. They are the weakest and not very cost-efficient.
    They don't want to learn how to fight, keep to themself, and don't invest in equipment. They are available from the start and are the only unit you can recruit from every culture.

Levies/Auxiliary: They are simple supports. They start with better eq and have one more step in progression than volunteers.
    That makes them usable even in the endgame especially since there can be no other unit that will fulfill their role.

Regulars: They are the most cost-efficient tree. Starting at tier 2 with decent eq and ending on tier 4 with tier 5 or even 6 equipment(single pieces only). They have good skills and equipment for their cost.
    That makes the core of every army and will be the biggest part of every party.
    
Mercenary: Available from settlements of a different culture. Cost the same as regulars but are slightly worst. Good if you are not in your territory and need quick recruits.
    But limited choice can make you starved for different troop types. Usually, they represent what a given culture likes the most, not what is the best at.
    However, sometimes they can fill gaps in your own culture. So consider picking some along your way.

Noble: They are limited in number, you can recruit them only from land owners and have higher requirements. Very high skills and most of their eq is tier 5 or better, they are even better than the vanilla versions.
    They are strong the extension of every army. Every lord will try to get them as much as he can if he is allowed to get them and you should too.
    You can recruit nobles of different cultures but need to have at least 80 relations with notable.

Elite: Costs even more than noble and have slightly worse eq and skills. But even so, they are still power you should be wary of.
    Together with nobles, they create the strongest army culture can field. But can you afford them? and is it worth the cost?
    They will do roles that levy or regular are doing, making them better but less cost-efficient replacements.
    They will however never copy nobles.

##Requirements for recruiting:

Volunteers:
There are no requirements for them.

Noble:
Clan tier: 4
Quartermaster steward skill min: 150
Scout scouting skill min: 35
One of the following three:
Player tactics skill min: 40
Player leadership skill min: 50
Surgeon medicine > 50 AND Engineer engineering > 50
When recruiting them from a different culture: Relation with notable > 80

Below are troops that will show up only in the settlements with the same culture that you have:

Levy:
Clan tier: 2
Quartermaster steward skill min: 50
Scout scouting skill min: 10
One of the following three:
Player tactics skill min: 10
Player leadership skill min: 10
Surgeon medicine > 20 AND Engineer engineering > 20


Regular:
Clan tier: 3
Quartermaster steward skill min: 100
Scout scouting skill min: 20
One of the following three:
Player tactics skill min: 30
Player leadership skill min: 30
Surgeon medicine > 40 AND Engineer engineering > 30

Elite:
Clan tier: 5
Quartermaster steward skill min: 200
Scout scouting skill min: 50
One of the following three:
Player tactics skill min: 50
Player leadership skill min: 70
Surgeon medicine > 80 AND Engineer engineering > 50

##Recruitment pool
Simply put it is a number of people that notable can give you.
There are two types:
    Normal - represents non-noble people. The current max amount available is 8. The computation of the number of recruits that notable will let you take is similar to vanilla.
    Noble - Used to recruit the noble type of units. Max is 4.
You cannot see the current number of available units only the number that is available to you.
So don't be surprised when you will left the village empty but lord next to you will be able to pull a few more recruits.

##Short guide
In the beginning, don't be afraid to travel, you can recruit units from all cultures. They can be weak but you won't be stranded without units to recruit.
Remember about requirements. Start leveling tactics or leadership or get someone to fullfill medicine and enginering req.
You can always recruit vanilla mercenaries.

On clan tier 2 you should stop recruiting volunteers and focus on levies and mercenaries.
Thanks to TW elites are not as expensive as I want them to be so they are a good choice if you can spare extra money.
And be careful of enemy noble better units especially noble and elite. They can wreck your party.

#Other stuff

##Perks
Perks that higher the number of recruits should work correctly.
The problem is other perks. Lowering the recruitment cost of archers won't work when there is no chance of them spawning, because troop choice is static.
The same can be said about horses but there are some of them to choose so... it's okay.
Since I'm filling the pool in my way some of those that rise the chance for horses or something will not work.

##Quests
Quests weren't changed so some of them can be hard to finish, like noble needs recruits.
And some may be hard. Caravan ambush comes to mind since it's hard to deal with calvary having only peasants in the party.
So save often and don't be mindless.

##Compatibility
Generally, all troop tree mods will not be working since this mod will hide them.
Mods that are using troops can try to pull vanilla troops or won't work. But generally, I was trying to be smart about my changes so there is a possibility.
Mods that are using/checking troop trees must create their compatibility patch so don't even ask for help if it doesn't work I won't be able to help.
Most other mods should work fine. I had a few of them installed during testing.

##AI
For many reasons, AI recruitment behavior can be broken.

##Installation
Just use vortex.

##Future
Rework troop trees - They are not finished I already have some ideas and see spots for improvement.
Better options for recruitment in settlements of different cultures - Traveling and spending time on land outside of your own culture
    is plentiful and you should not have to constantly return home to get some freshman that will die anyway.
Quest update - All quests work but their calculations are off. They can be easier or harder, depending on TW math.
AI improvement - Now recruitment is stupid and can cause problems. I'm planning some upgrades.

#Q&A
Q: If I found a problem what should I do?
A: If that's an AI problem describe and make a post. I give no promises that I will be able to fix it.
If a game crashes: get some callstack/logs and a description of you what were doing before it broke and post it.


Q: Some cultures seem weak.
A: Troop trees are still in progress, you can post your ideas.
But you should know that I want to make cultures more unique so ideas like: "Give access to every troop type on a regular level" are thrown out of the window.
Also, progression needs to be kept.


Q: Configuration?
A: NO.

Besides balance going out of the way and additional work, it will break my design and make me sad.
Just as Dark Souls should not have an easy mode, I don't want to let you change how you recruit.


Q: Will this mod be updated?
A: I will be updating if I will have time and morale. But cannot promise regular updates.
I only hope that TW will slow down with releasing patches so that updates will work as long as they can.


Q: I want to base my own on this framework. How can I do that?
A: C# programing skills are a must because of how stuff is implemented. Creating a way to define your troop trees and assigning them to troop types in XML would be possible But that's not what I want to do. I'm thinking about adding a way to use the framework with only the XML but I cannot promise anything, so your wait may be fruitless.
If you are ok with that check my repo:


All you need to do is to inherit CultureDecriptor. Just as I do. And add your modifications to "descriptorList" the same way I do. Additionally, some checks will make sure you set up everything right. You can find the checking function: JabBeh.CheckAllDescriptors().
You can contact me If you have any problems, suggestions or request a feature.