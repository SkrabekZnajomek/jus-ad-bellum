﻿using JusAdBellum.Utility;
using JusAdBellum.CultureUnitLists;
using System;
using System.Collections.Generic;
using TaleWorlds.CampaignSystem;
using TaleWorlds.CampaignSystem.ViewModelCollection;
using TaleWorlds.Core;
using TaleWorlds.Core.ViewModelCollection.Generic;
using TaleWorlds.Core.ViewModelCollection.Information;
using TaleWorlds.Library;

namespace JusAdBellum
{
    class VolunteerVM : ViewModel
    {
		public static Action<VolunteerVM> OnFocused;

        private readonly Action<VolunteerVM> _onClick;

        private readonly Action<VolunteerVM> _onRemoveFromCart;

        private CharacterObject _character;

        public CharacterObject Character;

        public int Index;

        private int _maximumIndexCanBeRecruit;

        private int _requiredRelation;

        public RowOfVolunteersVM Owner;

        private ImageIdentifierVM _imageIdentifier;

        private string _nameText;

        private string _level;

        private bool _canBeRecruited;

        private bool _isInCart;

        private int _wage;

        private int _cost;

        private bool _isTroopEmpty;

        private bool _playerHasEnoughRelation;

        private int _currentRelation;

        private bool _isHiglightEnabled;

        private StringItemWithHintVM _tierIconData;

        private StringItemWithHintVM _typeIconData;

        public UnitType UnitTier { get; set; }

        [DataSourceProperty]
        public string Level
        {
            get
            {
                return _level;
            }
            set
            {
                if (value != _level)
                {
                    _level = value;
                    OnPropertyChangedWithValue(value, "Level");
                }
            }
        }

        [DataSourceProperty]
        public bool CanBeRecruited
        {
            get
            {
                return _canBeRecruited;
            }
            set
            {
                if (value != _canBeRecruited)
                {
                    _canBeRecruited = value;
                    OnPropertyChangedWithValue(value, "CanBeRecruited");
                }
            }
        }

        [DataSourceProperty]
        public bool IsHiglightEnabled
        {
            get
            {
                return _isHiglightEnabled;
            }
            set
            {
                if (value != _isHiglightEnabled)
                {
                    _isHiglightEnabled = value;
                    OnPropertyChangedWithValue(value, "IsHiglightEnabled");
                }
            }
        }

        [DataSourceProperty]
        public int Wage
        {
            get
            {
                return _wage;
            }
            set
            {
                if (value != _wage)
                {
                    _wage = value;
                    OnPropertyChangedWithValue(value, "Wage");
                }
            }
        }

        [DataSourceProperty]
        public int Cost
        {
            get
            {
                return _cost;
            }
            set
            {
                if (value != _cost)
                {
                    _cost = value;
                    OnPropertyChangedWithValue(value, "Cost");
                }
            }
        }

        [DataSourceProperty]
        public bool IsInCart
        {
            get
            {
                return _isInCart;
            }
            set
            {
                if (value != _isInCart)
                {
                    _isInCart = value;
                    OnPropertyChangedWithValue(value, "IsInCart");
                }
            }
        }

        [DataSourceProperty]
        public bool IsTroopEmpty
        {
            get
            {
                return _isTroopEmpty;
            }
            set
            {
                if (value != _isTroopEmpty)
                {
                    _isTroopEmpty = value;
                    OnPropertyChangedWithValue(value, "IsTroopEmpty");
                }
            }
        }

        [DataSourceProperty]
        public bool PlayerHasEnoughRelation
        {
            get
            {
                return _playerHasEnoughRelation;
            }
            set
            {
                if (value != _playerHasEnoughRelation)
                {
                    _playerHasEnoughRelation = value;
                    OnPropertyChangedWithValue(value, "PlayerHasEnoughRelation");
                }
            }
        }

        [DataSourceProperty]
        public ImageIdentifierVM ImageIdentifier
        {
            get
            {
                return _imageIdentifier;
            }
            set
            {
                if (value != _imageIdentifier)
                {
                    _imageIdentifier = value;
                    OnPropertyChangedWithValue(value, "ImageIdentifier");
                }
            }
        }

        [DataSourceProperty]
        public string NameText
        {
            get
            {
                return _nameText;
            }
            set
            {
                if (value != _nameText)
                {
                    _nameText = value;
                    OnPropertyChangedWithValue(value, "NameText");
                }
            }
        }

        [DataSourceProperty]
        public StringItemWithHintVM TierIconData
        {
            get
            {
                return _tierIconData;
            }
            set
            {
                if (value != _tierIconData)
                {
                    _tierIconData = value;
                    OnPropertyChangedWithValue(value, "TierIconData");
                }
            }
        }

        [DataSourceProperty]
        public StringItemWithHintVM TypeIconData
        {
            get
            {
                return _typeIconData;
            }
            set
            {
                if (value != _typeIconData)
                {
                    _typeIconData = value;
                    OnPropertyChangedWithValue(value, "TypeIconData");
                }
            }
        }

        public VolunteerVM(RowOfVolunteersVM owner, CharacterObject character, int index, Action<VolunteerVM> onClick, Action<VolunteerVM> onRemoveFromCart)
        {
            if (character != null)
            {
                NameText = character.Name.ToString();
                _character = character;
                GameTexts.SetVariable("LEVEL", character.Level);
                Level = GameTexts.FindText("str_level").ToString();
                Character = character;
                Wage = Character.TroopWage;
                Cost = Campaign.Current.Models.PartyWageModel.GetTroopRecruitmentCost(Character, Hero.MainHero);
                IsTroopEmpty = false;
                CharacterCode characterCode = CampaignUIHelper.GetCharacterCode(character);
                ImageIdentifier = new ImageIdentifierVM(characterCode);
                TierIconData = CampaignUIHelper.GetCharacterTierData(character);
                TypeIconData = CampaignUIHelper.GetCharacterTypeData(character);
            }
            else
            {
                IsTroopEmpty = true;
            }

            Owner = owner;
            if (Owner != null)
            {
                _currentRelation = Hero.MainHero.GetRelation(Owner.OwnerHero);
            }

            _maximumIndexCanBeRecruit = Campaign.Current.Models.VolunteerModel.MaximumIndexHeroCanRecruitFromHero(Hero.MainHero, Owner.OwnerHero);
            for (int i = -100; i < 100; i++)
            {
                if (index < Campaign.Current.Models.VolunteerModel.MaximumIndexHeroCanRecruitFromHero(Hero.MainHero, Owner.OwnerHero, i))
                {
                    _requiredRelation = i;
                    break;
                }
            }

            _onClick = onClick;
            Index = index;
            _onRemoveFromCart = onRemoveFromCart;
            RefreshValues();
        }

        public override void RefreshValues()
        {
            base.RefreshValues();
            if (_character != null)
            {
                NameText = _character.Name.ToString();
                GameTexts.SetVariable("LEVEL", _character.Level);
                Level = GameTexts.FindText("str_level").ToString();
            }
        }

        public void ExecuteRecruit()
        {
            if (CanBeRecruited)
            {
                _onClick(this);
            }
            else if (IsInCart)
            {
                _onRemoveFromCart(this);
            }
        }

        public void ExecuteOpenEncyclopedia()
        {
            if (Character != null)
            {
                Campaign.Current.EncyclopediaManager.GoToLink(Character.EncyclopediaLink);
            }
        }

        public void ExecuteRemoveFromCart()
        {
            if (IsInCart)
            {
                _onRemoveFromCart(this);
            }
        }

        protected virtual void ExecuteBeginHint()
        {
            if (_character != null)
            {
                if (CanBeRecruited)
                {
                    InformationManager.ShowTooltip(typeof(CharacterObject), _character);
                    return;
                }

                List<TooltipProperty> list = new List<TooltipProperty>();
                string text = "";
                list.Add(new TooltipProperty(text, _character.Name.ToString(), 1));
                list.Add(new TooltipProperty(text, text, -1));
                GameTexts.SetVariable("LEVEL", _character.Level);
                GameTexts.SetVariable("newline", "\n");
                list.Add(new TooltipProperty(text, GameTexts.FindText("str_level").ToString(), 0));
                Jab.CD[Character.Culture].SetRequirments(UnitTier, Owner.OwnerHero);

                bool isNobleUnit = UnitTier == UnitType.NobleUnit;
                if ((isNobleUnit)? Owner.HaveNoblesAvailable() : Owner.HavePeasantsAvailable())
                    list.Add(new TooltipProperty(text, GameTexts.FindText("str_recruit_requirements_not_met").ToString(), 0));
                else if (isNobleUnit)
                    list.Add(new TooltipProperty(text, GameTexts.FindText("str_recruit_no_noble_recruits_available").ToString(), 0));
                else
                    list.Add(new TooltipProperty(text, GameTexts.FindText("str_recruit_no_normal_recruits_available").ToString(), 0));

                InformationManager.ShowTooltip(typeof(List<TooltipProperty>), list);

            }
            else
            {
                if (CanBeRecruited)
                {
                    if (Hero.MainHero.Culture == Owner.OwnerHero.Culture)
                        MBInformationManager.ShowHint(GameTexts.FindText("str_recruit_no_type_same_culture").ToString());
                    else
                        MBInformationManager.ShowHint(GameTexts.FindText("str_recruit_no_type_different_culture").ToString());

                    return;
                }

                if (Hero.MainHero.Culture == Owner.OwnerHero.Culture)
                    MBInformationManager.ShowHint(GameTexts.FindText("str_recruit_no_type_same_culture").ToString());
                else
                    MBInformationManager.ShowHint(GameTexts.FindText("str_recruit_no_type_different_culture").ToString());
            }
        }

        protected virtual void ExecuteEndHint()
        {
            MBInformationManager.HideInformations();
        }

		public void ExecuteFocus()
		{
			if (!this.IsTroopEmpty)
			{
				Action<VolunteerVM> onFocused = VolunteerVM.OnFocused;
				if (onFocused == null)
				{
					return;
				}
				onFocused(this);
			}
		}

		public void ExecuteUnfocus()
		{
			Action<VolunteerVM> onFocused = VolunteerVM.OnFocused;
			if (onFocused == null)
			{
				return;
			}
			onFocused(null);
		}
    }
}
