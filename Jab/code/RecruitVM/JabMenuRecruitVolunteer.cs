﻿using System.Linq;
using JusAdBellum.Utility;
using SandBox.View.Map;
using SandBox.View.Menu;
using TaleWorlds.Core;
using TaleWorlds.Engine.GauntletUI;
using TaleWorlds.GauntletUI.Data;
using TaleWorlds.InputSystem;
using TaleWorlds.Library;
using TaleWorlds.MountAndBlade.View;
using TaleWorlds.ScreenSystem;

namespace JusAdBellum
{
    [OverrideView(typeof(MenuRecruitVolunteersView))]
    class JabMenuRecruitVolunteer : MenuView
	{
		protected override void OnInitialize()
		{
			JL.Log(JLT.RecruitmentUI, "running JabMenuRectuitVolunteer");

			base.OnInitialize();
			_dataSource = new JabRecruitmentVM();
			_dataSource.SetCancelInputKey(HotKeyManager.GetCategory("GenericPanelGameKeyCategory").RegisteredHotKeys.FirstOrDefault((HotKey g) => ((g != null) ? g.Id : null) == "Exit"));
			_dataSource.SetDoneInputKey(HotKeyManager.GetCategory("GenericPanelGameKeyCategory").RegisteredHotKeys.FirstOrDefault((HotKey g) => ((g != null) ? g.Id : null) == "Confirm"));
			_dataSource.SetResetInputKey(HotKeyManager.GetCategory("GenericPanelGameKeyCategory").GetHotKey("Reset"));
			_dataSource.SetRecruitAllInputKey(HotKeyManager.GetCategory("GenericPanelGameKeyCategory").RegisteredHotKeys.FirstOrDefault((HotKey g) => ((g != null) ? g.Id : null) == "TakeAll"));
			Layer = new GauntletLayer(206, "GauntletLayer")
			{
				Name = "RecuritLayer"
			};
			_layerAsGauntletLayer = (Layer as GauntletLayer);
			Layer.InputRestrictions.SetInputRestrictions(true, InputUsageMask.All);
			MenuViewContext.AddLayer(Layer);
			Layer.Input.RegisterHotKeyCategory(HotKeyManager.GetCategory("GenericPanelGameKeyCategory"));
			Layer.Input.RegisterHotKeyCategory(HotKeyManager.GetCategory("GenericCampaignPanelsGameKeyCategory"));
			_movie = _layerAsGauntletLayer.LoadMovie("RecruitmentPopup", _dataSource);
			Layer.IsFocusLayer = true;
			ScreenManager.TrySetFocus(Layer);
			_dataSource.RefreshScreen();
			_dataSource.Enabled = true;
			Game.Current.EventManager.TriggerEvent<TutorialContextChangedEvent>(new TutorialContextChangedEvent(TutorialContexts.RecruitmentWindow));
			MapScreen mapScreen;
			if ((mapScreen = (ScreenManager.TopScreen as MapScreen)) != null)
			{
				mapScreen.SetIsInRecruitment(true);
			}
		}

		protected override void OnFinalize()
		{
			_layerAsGauntletLayer.IsFocusLayer = false;
			ScreenManager.TryLoseFocus(_layerAsGauntletLayer);
			_dataSource.OnFinalize();
			_dataSource = null;
			_layerAsGauntletLayer.ReleaseMovie(_movie);
			MenuViewContext.RemoveLayer(_layerAsGauntletLayer);
			_movie = null;
			Layer = null;
			_layerAsGauntletLayer = null;
			Game.Current.EventManager.TriggerEvent<TutorialContextChangedEvent>(new TutorialContextChangedEvent(TutorialContexts.MapWindow));
			MapScreen mapScreen;
			if ((mapScreen = (ScreenManager.TopScreen as MapScreen)) != null)
			{
				mapScreen.SetIsInRecruitment(true);
			}
			base.OnFinalize();
		}

		protected override void OnFrameTick(float dt)
		{
			base.OnFrameTick(dt);
			if (_layerAsGauntletLayer.Input.IsHotKeyReleased("Exit"))
			{
				_dataSource.ExecuteForceQuit();
			}
			else if (_layerAsGauntletLayer.Input.IsHotKeyReleased("Confirm"))
			{
				_dataSource.ExecuteDone();
			}
			else if (Layer.Input.IsHotKeyReleased("Reset"))
			{
				_dataSource.ExecuteReset();
			}
			else if (Layer.Input.IsHotKeyReleased("TakeAll"))
			{
				_dataSource.ExecuteRecruitAll();
			}
			else if (Layer.Input.IsGameKeyReleased(39))
			{
				if (_dataSource.FocusedVolunteerOwner != null)
				{
					_dataSource.FocusedVolunteerOwner.ExecuteOpenEncyclopedia();
				}
				else if (_dataSource.FocusedVolunteerTroop != null)
				{
					_dataSource.FocusedVolunteerTroop.ExecuteOpenEncyclopedia();
				}
			}
			if (!_dataSource.Enabled)
			{
				MenuViewContext.CloseRecruitVolunteers();
			}
		}

		private GauntletLayer _layerAsGauntletLayer;

		private JabRecruitmentVM _dataSource;

		private IGauntletMovie _movie;
	}
}
