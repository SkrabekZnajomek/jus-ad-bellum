﻿using System;
using System.Collections.Generic;
using System.Linq;
using HarmonyLib;
using TaleWorlds.MountAndBlade;
using TaleWorlds.CampaignSystem;
using TaleWorlds.Core;
using TaleWorlds.ObjectSystem;
using JusAdBellum.Utility;
using JusAdBellum.CultureUnitLists;

/*
    Unit changes ideas:
        Need to debug all units.
        Update all eq for it can be outdate due to in game changes.
        Do something with wages.
        And With exp needed for t6 troops to get to t7.

    Tell players what req they dont fill
*/

//FIXME BasicTroop override is hardcoded and its references are not check and can lead to weird parties being created
//FIXME PlayerTownVisitCampaignBehaviorPatches
//FIXME Calvary tactics bonus for calvary may be wrong.
//FIXME sometimes all notables from settlment share the same filed in dictionary probably due to hash change.

namespace JusAdBellum
{
    class JabBeh : CampaignBehaviorBase
    {
        public Dictionary<Hero, Volunteers> numberOfVolunteers = new Dictionary<Hero, Volunteers>();

        public Dictionary<CultureObject, CultureDescriptor> cultureDescriptors = new Dictionary<CultureObject, CultureDescriptor>();

        public void BuildDescriptorsDictionary(List<CultureDescriptor> descriptorList)
        {
            MBObjectManager objectManager = MBObjectManager.Instance;
            List<CultureObject> cultures = objectManager.GetObjectTypeList<CultureObject>().ToList<CultureObject>();
            foreach (var descriptor in descriptorList)
            {
                descriptor.BuildCharacterList();
                cultureDescriptors.Add(cultures.Find(x => x.Name.ToString() == descriptor.CultureName), descriptor);
            }
        }

        public override void RegisterEvents()
        {
        }

        public override void SyncData(IDataStore dataStore)
        {
            dataStore.SyncData("numberOfVolunteers", ref numberOfVolunteers);
            JL.Log(JLT.Core, $"Loaded directory from save file, count: {numberOfVolunteers.Count} ");
        }

        internal void CheckAllDescriptors()
        {
            Dictionary<CultureObject, CultureDescriptor>.Enumerator itr = cultureDescriptors.GetEnumerator();
            CultureObject looters = MBObjectManager.Instance.GetObjectTypeList<CultureObject>().ToList<CultureObject>().Find(x => x.Name.ToString() == "looters");
            while (itr.MoveNext())
            {
                float[] weights = null;
                for (int i = 0; i < 7; i++)
                {
                    weights = itr.Current.Value.GetDispositionForClan(i, itr.Current.Key);
                    if (weights.Sum() != 1.0f)
                        throw new Exception($"disposition does not equal 1, Clan Culture: {itr.Current.Key.Name}, clan tier: {i}");

                    for (int j = 0; j < weights.Length; j++)
                    {
                        if (weights[j] > 0.0f && itr.Current.Value.GetUnitFromTier((UnitType) j) == null)
                            throw new Exception($"Noble can recruit non-existing unit, Culture: {itr.Current.Key.Name}, clan tier: {i}, Unit type: {(UnitType) j}");
                    }
                }


                weights = itr.Current.Value.GetDispositionForClan(0, looters);
                if (weights.Sum() != 1.0f)
                    throw new Exception($"disposition does not equal 1 for mercenary, Culture: {itr.Current.Key.Name}, clan tier: {0}");

                for (int j = 0; j < weights.Length; j++)
                {
                    if (weights[j] > 0.0f && itr.Current.Value.GetUnitFromTier((UnitType) j) == null)
                        throw new Exception($"Noble can recruit non-existing unit for different culture, Culture: {itr.Current.Key.Name}, clan tier: {0}, Unit type: {(UnitType) j}");
                }

            }
        }

        internal void ClearCultureDescriptors()
        {
            Dictionary<CultureObject, CultureDescriptor>.Enumerator itr = cultureDescriptors.GetEnumerator();
            while (itr.MoveNext())
            {
                itr.Current.Value.ClearUnits();
            }

            cultureDescriptors.Clear();
        }
    }

    public class Jab : MBSubModuleBase
    {
        public static Dictionary<Hero, Volunteers> Nov { get => CoreBehavior.numberOfVolunteers; set => CoreBehavior.numberOfVolunteers = value; }
        public static Dictionary<CultureObject, CultureDescriptor> CD { get => CoreBehavior.cultureDescriptors; set => CoreBehavior.cultureDescriptors = value; }


        public static List<CultureDescriptor> descriptorList = new List<CultureDescriptor>();
        private static readonly JabBeh CoreBehavior = new JabBeh();


        public static int MAX_PESANT_RECRUITS = 8, MAX_NOBLE_RECRUITS = 4;
        private readonly Harmony H = new Harmony("mod.bannerlord.jus_ad_bellum");



        protected override void OnGameStart(Game game, IGameStarter gameStarterObject)
        {
            if (gameStarterObject is CampaignGameStarter starter)
            {
                starter.AddBehavior(CoreBehavior);
            }
        }

        public override bool DoLoading(Game game)
        {
            if (game.GameType is Campaign)
            {
                CoreBehavior.BuildDescriptorsDictionary(descriptorList);
                CoreBehavior.CheckAllDescriptors();
            }
            return true;
        }

        protected override void OnSubModuleLoad()
        {
            // Harmony.DEBUG = true;
            // FileLog.Reset();
            H.PatchAll();
        }

        public override void OnGameEnd(Game game)
        {
            JL.Log(JLT.Core, $"cleared numberOfVolunteers {Nov.Count}");
            CoreBehavior.ClearCultureDescriptors();
            Nov.Clear();
        }
    }
}