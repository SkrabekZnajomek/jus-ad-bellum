﻿using HarmonyLib;
using JusAdBellum.Utility;
using TaleWorlds.CampaignSystem;

namespace JusAdBellum
{
    [HarmonyPatch(typeof(Hero))]
    public class HeroPatch
    {
        public static void AddNumberOfVolunteersAndVolunteersToNotable(Hero notable)
        {
            if (notable.CurrentSettlement != null && notable.PartyBelongedTo == null && notable.PartyBelongedToAsPrisoner == null)
            {
                if (notable.IsNotable && notable.CanHaveRecruits)
                {
                    try
                    {
                        Jab.Nov.Add(notable, new Volunteers());
                        JL.Log(JLT.numberOfVolunteers, $"added number of volunteers to {notable.Name}, current count: {Jab.Nov.Count}");
                    }
                    catch 
                    { 
                        JL.Log(JLT.numberOfVolunteers, $"notable allready exist in dictionary, name: {notable.Name}, current count: {Jab.Nov.Count}"); 
                    }
                }
            }
        }

        [HarmonyPostfix]
        [HarmonyPatch("StayingInSettlement", MethodType.Setter)]
        static void StayingInSettlementOfNotablePostfix(Hero __instance)
        {
            AddNumberOfVolunteersAndVolunteersToNotable(__instance);
        }
    }
}
